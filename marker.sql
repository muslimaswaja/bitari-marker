-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 08, 2017 at 08:05 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `marker`
--

-- --------------------------------------------------------

--
-- Table structure for table `marker`
--

CREATE TABLE IF NOT EXISTS `marker` (
  `nomor` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `deskripsi` tinytext NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `marker`
--

INSERT INTO `marker` (`nomor`, `nama`, `jenis`, `deskripsi`, `lat`, `lng`) VALUES
(18, 'Politeknik Negeri Jember', 'universitas', 'Politeknik Negeri Jember (POLIJE), adalah salah satu perguruan tinggi negeri yang terdapat di Kabupaten Jember, Provinsi Jawa Timur, Indonesia.', -8.159653145212413, 113.7236239752383),
(19, 'RSUD Doktor Soebandi', 'hospital', 'RSU Dr Soebandi adalah rumah sakit negeri kelas B. Rumah sakit ini mampu memberikan pelayanan kedokteran spesialis dan subspesialis terbatas. Rumah sakit ini juga menampung pelayanan rujukan dari rumah sakit kabupaten.', -8.151312496389588, 113.71570598290418),
(20, 'Masjid Jami Al Baitul Amien', 'masjid', ' Masjid Jami Al Baitul Amien, masjid tua warisan masa kolonial Belanda ini masih kokoh berdiri di jantung Kota Jember, Jawa Timur. Keunikannya tak hanya berhenti di situ. Arsitektur masjid ini berbeda dari kebanyakan tempat ibadah lainnya dan yang paling ', -8.168198644590728, 113.70046030751837);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
