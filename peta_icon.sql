-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Waktu pembuatan: 14. Juli 2013 jam 23:13
-- Versi Server: 5.1.41
-- Versi PHP: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `peta_icon`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `peta_icon`
--

CREATE TABLE IF NOT EXISTS `peta_icon` (
  `nomor` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `deskripsi` tinytext NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  PRIMARY KEY (`nomor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data untuk tabel `peta_icon`
--

INSERT INTO `peta_icon` (`nomor`, `nama`, `jenis`, `deskripsi`, `lat`, `lng`) VALUES
(18, 'Masjid Raya Klaten', 'masjid', 'Masjid terbesar di kota Klaten', -7.70435734756343, 110.603485107422),
(19, 'Sop Ayam Pak Min Klaten', 'restoran', 'Sop ayam legendaris', -7.70963078024435, 110.594730377197),
(20, 'Pemancingan Janti', 'piknik', 'Pemancingan terbesar di jawa tengah', -7.71813617835422, 110.60245513916),
(21, 'GOR Gelarsena', 'piknik', 'GOR terbesar se klaten', -7.69398040125062, 110.614128112793),
(23, 'OW Deles Indah', 'piknik', 'Disini bisa lihat Gunung Merapi', -7.69517121127151, 110.574817657471);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
