<!DOCTYPE html>
<html>
	<head>
		<title>Bitari Marker</title>
		
		<!-- Style -->
		<link rel="stylesheet" type="text/css" href="css.css">
		
		<style>
			body {
				background-color: #87cefa;
			}
			
			#jendelainfo {
				position: absolute;
				z-index: 800;
				top: 100;
				left: 400;
				background-color: yellow;
				display: none;
			}
		</style>
		
		<!-- Javascript -->
		<script type="text/javascript" src="jquery-1.4.3.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZJj0Y4rjeYAJQia1OnG_7qM08I0HDRwY&libraries=places&callback=initMap" async defer></script>
	</head>

	<body>
		<center>
			<font face="ar darling" size="7">MAPS </font> 
			<font face="ar darling" size="7"> MARKER</font>
			
			<input id="pac-input" class="controls" type="text" placeholder="Search Box">
		</center>

		<table id="jendelainfo" border="1" cellpadding="4" cellspacing="0" style="border-collapse: collapse" bordercolor="#FFCC00" width="250" height="136">
			<tr>
				<td width="248" bgcolor="87CEFA" height="19">
					<font color=white>
						<span id="teksjudul"></span>
					</font>
				</td>
				
				<td width="30" bgcolor="87CEFA" height="19">
					<p align="center">
						<font color="RED">
							<a style="cursor:pointer" id="tutup" title="Tutup">
								<b><img src="icon/close.png"></b>
							</a>
						</font>
					</p>
				</td>
			</tr>
			
			<tr>
				<td width="290" bgcolor="#FFCC00" height="100" valign="top" colspan="2">
					<p align="left">
						<span id="teksdes"></span>
					</p>
				</td>
			</tr>
		</table>
		
		<table border=1 >
			<tr>
				<td>
					<div id="petaku" style="width:1050px; height:600px"></div>
				</td>
				
				<td  valign=top align=left bgcolor="#87CEFA">
					<font size="5">
						<hr>
						<b>Masukkan Data Marker Disini</b>
					</font>
					
					<p>
						<hr>
						
						<font size="3">
							<br>
							
							<b>Pilih jenis lokasi:</b>
							
							<br>
						</font>
					</p>
					
					<input type=radio checked name=jenis value="restoran" onclick="setjenis(this.value)"><img src="icon/restaurant1.png">Restoran<font size="2" color="#87CEFA">haf</font>  
					<input type=radio name=jenis value="mall" onclick="setjenis(this.value)"><img src="icon/mall.png">Mall<br>
					<input type=radio name=jenis value="masjid" onclick="setjenis(this.value)"><img src="icon/mosque.png">Masjid <font size="2" color="#87CEFA">haffff</font>
					<input type=radio name=jenis value="hospital" onclick="setjenis(this.value)"><img src="icon/hospital.png">Hospital<br>
					<input type=radio name=jenis value="universitas" onclick="setjenis(this.value)"><img src="icon/university.png"> Universitas
					<input type=radio name=jenis value="volcano" onclick="setjenis(this.value)"><img src="icon/volcano.png"> Gunung<br>
					
					<p>
						<hr>
						
						<font size="2">(klik lokasi untuk menentukan koordinat lat dan lng)</font>
						
						<br>
					</p>
					
					Lat : <input type=text id=x>
					<br>
					Lng : <input type=text id=y>
					<br>
					Nama Lokasi:<input type=text id="judul" size=35>
					<br>
					Deskripsi:<br>
					
					<textarea cols=37 rows=6 id="deskripsi"></textarea>
					
					<button id="tombol_simpan">SIMPAN</button> 
					
					<a href="tabel_db.php">
						<p>Lihat Data</p>
					</a>
					
					<img src="icon/ajax-loader.gif" style="display:none" id="loading">
				</td>
			</tr>
		</table>
		
		<br>
		<script>
			var peta;
			var pertama = 0;
			var jenis = "restoran";
			var judulx = new Array();
			var desx = new Array();
			var i;
			var url;
			var gambar_tanda;
			
			function initMap() {
				var center = new google.maps.LatLng(-7.276760993414493, 112.79490351676941);
				var petaoption = {
					zoom: 10,
					center: center,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				peta = new google.maps.Map(document.getElementById("petaku"), petaoption);
				
				google.maps.event.addListener(peta, 'click', function (event) {
					kasihtanda(event.latLng);
				});
				
				ambildatabase('awal');
				
				var input = document.getElementById('pac-input');
				var searchBox = new google.maps.places.SearchBox(input);
				peta.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

				// Bias the SearchBox results towards current map's viewport.
				peta.addListener('bounds_changed', function () {
					searchBox.setBounds(peta.getBounds());
				});

				var markers = [];
				// Listen for the event fired when the user selects a prediction and retrieve
				// more details for that place.
				searchBox.addListener('places_changed', function () {
					var places = searchBox.getPlaces();

					if (places.length == 0) {
						return;
					}

					// Clear out the old markers.
					markers.forEach(function (marker) {
						marker.setMap(null);
					});
					markers = [];

					// For each place, get the icon, name and location.
					var bounds = new google.maps.LatLngBounds();
					places.forEach(function (place) {
						if (!place.geometry) {
							console.log("Returned place contains no geometry");
							return;
						}
						var icon = {
							url: place.icon,
							size: new google.maps.Size(71, 71),
							origin: new google.maps.Point(0, 0),
							anchor: new google.maps.Point(17, 34),
							scaledSize: new google.maps.Size(25, 25)
						};

						// Create a marker for each place.
						markers.push(new google.maps.Marker({
							map: peta,
							icon: icon,
							title: place.name,
							position: place.geometry.location
						}));

						if (place.geometry.viewport) {
							// Only geocodes have viewport.
							bounds.union(place.geometry.viewport);
						} else {
							bounds.extend(place.geometry.location);
						}
					});
					
					peta.fitBounds(bounds);
				});
			}

			$(document).ready(function () {
				$("#search").click(function () {
					var address = $("#address").val();
					alert("Yo yo");
				});

				$("#tombol_simpan").click(function () {
					var x = $("#x").val();
					var y = $("#y").val();
					var judul = $("#judul").val();
					var des = $("#deskripsi").val();
					$("#loading").show();
					$.ajax({
						url: "simpanlokasi.php",
						data: "x=" + x + "&y=" + y + "&judul=" + judul + "&des=" + des + "&jenis=" + jenis,
						cache: false,
						success: function (msg) {
							alert(msg);
							$("#loading").hide();
							$("#x").val("");
							$("#y").val("");
							$("#judul").val("");
							$("#deskripsi").val("");
							ambildatabase('akhir');
						}
					});
				});
				
				$("#tutup").click(function () {
					$("#jendelainfo").fadeOut();
				});
			});
			
			function kasihtanda(lokasi) {
				set_icon(jenis);
				
				tanda = new google.maps.Marker({
					position: lokasi,
					map: peta,
					icon: gambar_tanda
				});
				
				$("#x").val(lokasi.lat());
				$("#y").val(lokasi.lng());

			}

			function set_icon(jenisnya) {
				switch (jenisnya) {
					case "restoran":
						gambar_tanda = 'icon/restaurant1.png';
						break;
					case "mall":
						gambar_tanda = 'icon/mall.png';
						break;
					case  "masjid":
						gambar_tanda = 'icon/mosquee.png';
						break;
					case  "hospital":
						gambar_tanda = 'icon/hospital.png';
						break;
					case  "universitas":
						gambar_tanda = 'icon/university.png';
						break;
					case  "volcano":
						gambar_tanda = 'icon/volcano.png';
						break;
				}
			}

			function ambildatabase(akhir) {
				if (akhir == "akhir") {
					url = "ambildata.php?akhir=1";
				} else {
					url = "ambildata.php?akhir=0";
				}
				$.ajax({
					url: url,
					dataType: 'json',
					cache: false,
					success: function (msg) {
						for (i = 0; i < msg.wilayah.petak.length; i++) {
							judulx[i] = msg.wilayah.petak[i].judul;
							desx[i] = msg.wilayah.petak[i].deskripsi;

							set_icon(msg.wilayah.petak[i].jenis);
							var point = new google.maps.LatLng(
									parseFloat(msg.wilayah.petak[i].x),
									parseFloat(msg.wilayah.petak[i].y));
							tanda = new google.maps.Marker({
								position: point,
								map: peta,
								icon: gambar_tanda
							});
							setinfo(tanda, i);
						}
					}
				});
			}

			function setjenis(jns) {
				jenis = jns;
			}

			function setinfo(petak, nomor) {
				google.maps.event.addListener(petak, 'click', function () {
					$("#jendelainfo").fadeIn();
					$("#teksjudul").html(judulx[nomor]);
					$("#teksdes").html(desx[nomor]);
				});
			}
		</script>
	</body>
</html>
