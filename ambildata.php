<?php

include "koneksi.php";

$jenis = str_replace("-","",$_GET['jenis']);
$halaman = $_GET['halaman'];
$data = $_GET['data'];

if($data == "list") {
	if($jenis != "semua") {
		$sql_query = mysqli_query($koneksi, "SELECT * FROM marker WHERE jenis='$jenis' LIMIT 10 OFFSET 0");
	} else {
		$sql_query = mysqli_query($koneksi, "SELECT * FROM marker LIMIT 10 OFFSET 0");
	}
	
	echo getDataFromQuery($sql_query);
} else if($data == "jumlah") {
	if($jenis != "semua") {
		$sql_query = mysqli_query($koneksi, "SELECT COUNT(id) FROM marker WHERE jenis='$jenis'");
	} else {
		$sql_query = mysqli_query($koneksi, "SELECT COUNT(id) FROM marker");
	}
	
	echo getDataFromQuery($sql_query);
}

function getDataFromQuery($query) {
	$data = mysqli_fetch_all($query);
	$json = json_encode($data);
	
	return $json;
}

?>
