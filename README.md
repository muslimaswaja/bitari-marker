# Bitari Marker - Point Marker Google Map

Aplikasi ini digunakan untuk melakukan marking point. Data marking tersebut nantinya bisa diekspor
menjadi file .csv. Data peta yang digunakan diambil dari Google Map.

## Author
- Bitari Yulianto (Pembuat)
- Ahmadha Priyanggara
- Muslim Aswaja

## Instalasi
Clone repository ini dalam direktori web server Anda.

## Cara Berkontribusi
Untuk sementara ini, kami belum menerima kontribusi dari pihak luar.